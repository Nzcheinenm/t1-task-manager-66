package ru.t1.dkononov.tm.mapper;

import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.entity.model.Task;

public class TaskMapper {

    public static TaskDto taskToDto(final Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setDateFinish(task.getDateFinish());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDescription(task.getDescription());
        taskDto.setName(task.getName());
        taskDto.setId(task.getId());
        taskDto.setProjectId(task.getProjectId());
        return taskDto;
    }

    public static Task dtoToTask(final TaskDto taskDto) {
        Task task = new Task();
        task.setDateFinish(taskDto.getDateFinish());
        task.setDateStart(taskDto.getDateStart());
        task.setDescription(taskDto.getDescription());
        task.setName(taskDto.getName());
        task.setId(taskDto.getId());
        task.setProjectId(taskDto.getProjectId());
        return task;
    }

}

package ru.t1.dkononov.tm.mapper;

import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.model.Project;

public class ProjectMapper {

    public static ProjectDto projectToDto(final Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setDateFinish(project.getDateFinish());
        projectDto.setDateStart(project.getDateStart());
        projectDto.setDescription(project.getDescription());
        projectDto.setName(project.getName());
        projectDto.setId(project.getId());
        return projectDto;
    }

    public static Project dtoToProject(final ProjectDto projectDto) {
        Project project = new Project();
        project.setDateFinish(projectDto.getDateFinish());
        project.setDateStart(projectDto.getDateStart());
        project.setDescription(projectDto.getDescription());
        project.setName(projectDto.getName());
        project.setId(projectDto.getId());
        return project;
    }

}

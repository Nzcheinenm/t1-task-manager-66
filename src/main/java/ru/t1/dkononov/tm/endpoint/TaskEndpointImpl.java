package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.api.endpoint.TaskRestEndpoint;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskDtoService taskService;

    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskService.findAll();
    }

    @Override
    @PostMapping("/save")
    public TaskDto save(@RequestBody TaskDto task) {
        return taskService.save(task);
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/exsitsById/{id}")
    public boolean exsistsById(@PathVariable("id") String id) {
        return taskService.exsistsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void deleteById(@RequestBody TaskDto task) {
        taskService.deleteById(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteById(@RequestBody List<TaskDto> tasks) {
        taskService.deleteById(tasks);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}

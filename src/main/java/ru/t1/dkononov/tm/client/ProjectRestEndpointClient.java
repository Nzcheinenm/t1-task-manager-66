package ru.t1.dkononov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.model.Project;

import java.util.List;

public interface ProjectRestEndpointClient {

    public static final String BASE_URL = "http://localhost:8080/api/projects/";

    static ProjectRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class,BASE_URL);
    }

    @PutMapping("save")
    void save(@RequestBody Project project);

    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @PostMapping("/save")
    ProjectDto save(@RequestBody ProjectDto project);

    @GetMapping("/findById/{id}")
    ProjectDto findById(@PathVariable("id") String id);

    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDto project);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<ProjectDto> projects);

    @PostMapping("/clear")
    void clear();


}

package ru.t1.dkononov.tm.api.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import java.util.List;

public interface ProjectDtoService {
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @PostMapping("/save")
    ProjectDto save(@RequestBody ProjectDto project);

    @PostMapping("/save")
    ProjectDto save();

    @GetMapping("/findById/{id}")
    ProjectDto findById(@PathVariable("id") String id);

    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDto project);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<ProjectDto> projects);

    @PostMapping("/clear")
    void clear();
}
